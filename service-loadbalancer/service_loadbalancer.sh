#!/bin/bash

CONFIG_FOLDER=${KUBECONFIG%/*}
if [ -e "$CONFIG_FOLDER" ]; then
  if [ -e "$CONFIG_FOLDER/client.crt" ] && [ -e "$CONFIG_FOLDER/client.key" ]; then
    cat "$CONFIG_FOLDER/client.crt" "$CONFIG_FOLDER/client.key" > "$CONFIG_FOLDER/client.pem"
  fi
fi

for i in "$@"
do
case $i in
    --ssl-cert=*)
    SSL_CERT="${i#*=}"
    ;;
esac
done

if [ -n "$SSL_CERT" ]; then
  SSL_CERT_FOLDER=${SSL_CERT%/*}
  if [ -e "$SSL_CERT_FOLDER/fullchain.pem" ] && [ -e "$SSL_CERT_FOLDER/privkey.pem" ]; then
    cat "$SSL_CERT_FOLDER/fullchain.pem" "$SSL_CERT_FOLDER/privkey.pem" > "$SSL_CERT"
  fi
fi

/service_loadbalancer "$@"
